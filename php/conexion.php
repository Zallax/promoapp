<?php

require_once 'sorteos.php'; 
require_once 'config.php';
require_once 'mails.php';

if(!empty($_POST)){
	extract($_POST);

	if($funcion=="comprobarNum"){
		comprobarNumero($num);
		
	}

	if($funcion=="comprobarMail"){
		comprobarMail($correo);
		
	}
	if($funcion=="comprobarCodigo"){
		comprobarCodigo($cod);
		
	}

	if($funcion=="registrarDatos"){
		registrarDatos($datos);
		
	}

	if($funcion=="getDatos"){
		getDatos($telefono);
		
	}
}


function getDatos($telefono){
	echo json_encode(consulta("select * from participantes where telefono='$telefono'"));
}



function comprobarNumero($num){
	
	$consulta = "select * from participantes where telefono=:num;";
	$miconexion=connectDB();
	$statement = $miconexion->prepare($consulta);
	$statement ->setFetchMode(PDO::FETCH_ASSOC);
	$statement->bindParam(':num', $num, PDO::PARAM_STR);
	if(!$statement->execute()){
		echo "ERROR";
		$miconexion->close();
	}
	$datos=[];
	while($fila=$statement->fetch())
	{
		$datos[]=$fila;
	}
	if(count($datos)!=0){
		echo "existe";	
	} else {
		echo "no-existe";
	}
	
}


function comprobarCodigo($cod){
	
	$consulta = consulta("select codigo from codigos where codigo = '$cod';");
//	echo "select codigo from codigos where codigo = '$cod';";
	if(empty($consulta)){
		echo "no-existe";
	} else {
		$consulta2 = consulta("select cod_juego from participantes where cod_juego = '$cod';");
		if(!empty($consulta2)){
			echo "utilizado";
		} else {
			echo "valido";
		}
	} 

	
}

function comprobarCodigo2($cod){
	
	$consulta = consulta("select codigo from codigos where codigo = '$cod';");
//	echo "select codigo from codigos where codigo = '$cod';";
	if(empty($consulta)){
		return false;
	} else {
		$consulta2 = consulta("select cod_juego from participantes where cod_juego = '$cod';");
		if(!empty($consulta2)){
			return false;
		} else {
			return true;
		}
	} 

	
}



function connectDB()
{
    try
    {
        $opc=array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
        $dsn="mysql:host=localhost;dbname=promo_app_utrera";
        $usuario="root";
        $contrasena="root";
        $base=new PDO($dsn,$usuario,$contrasena,$opc);
    }
    catch (PDOException $e)
    {
        die ("Error".$e->getMessage());
        $resultado=null;
    }
    return $base;
}

function ejecutaConsulta($sql)
{
		//recibe una cadena conteniendo una instruccion SELECT y devuelve un resultset

		$miconexion=connectDB();
		return $miconexion->query($sql);

}

function consulta($sql)
{

		//recibe una cadena conteniendo una instruccion SELECT y devuelve un array con la fila de datos
		$datos=[];
		$resultset=ejecutaConsulta($sql);
		while($fila=$resultset->fetch(PDO::FETCH_ASSOC))
		{
			$datos[]=$fila;
		}
		return $datos;


}


function insert($sql)
{
		/*recibe una cadena conteniendo una instruccion DML, la ejecuta y
		devuelve el nº de filas afectadas por dicha instruccion*/
		$miconexion=connectDB();
		$accion = $miconexion->prepare($sql);
		$accion->execute();
		return $accion->rowCount();
		//return "1";
}

function comprobarMail($correo){
	$email = $correo;
	$vmail = new verifyEmail();
	        if ($vmail->check($email)) {
	            echo 1;
	        } elseif ($vmail->isValid($email)) {
	            echo 2;
	        } else {
	            echo 0;
	        }
}

function registrarDatos($datos){
	$aDatos = json_decode($datos, true);
	extract($aDatos);
	$valido = true;
	

	if(ISSET($codigo)){
		if(!comprobarCodigo2($codigo)){
			$valido = false;
		}
	}

	if($valido){
	global $asuntoMail, $color, $nombreEmpresa, $mailEmpresa, $mailEmpresa, $txtMailGanador;

	
	//$a2 = json_decode($aDatos);
	
	
	if(!ISSET($codigo)){
		
		$codigo = "-";


	} 
	$sorteo = hacerSorteo();

	if(is_array($sorteo)){
	$premio = $sorteo[1];
	$estadoMail = "no";
	if($premio!="-"){
		$mail = enviarMail($email, $premio, $asuntoMail, $color, $nombreEmpresa, $mailEmpresa, $codigo, $txtMailGanador);
		enviarCopia("desarrollo@marujalimon.com", $premio, "Nuevo ganador en ".$nombreEmpresa, $nombre, $nombreEmpresa, $mailEmpresa, $codigo);
		$estadoMail = "si";
		if($mail=="error"){
			$valido = false;
			$estadoMail = "no";
		}
	} else {
		global $asuntoMailPerdedor;
		$mailP = enviarMailPerdedor($email, $premio, $asuntoMailPerdedor, $color, $nombreEmpresa, $mailEmpresa);
		$estadoMail = "si";
		if($mailP=="error"){
			$valido = false;
			$estadoMail = "no";
		}
	}
	
 	
	$cod_canjeo = "-";

	$idPremio = $sorteo[0];

	

	$consulta = "insert into participantes (cod_juego, cod_canjeo, premio, email, nombre, telefono, edad, municipio, cod_postal, direccion, fecha_jugada, canjeado, estado_mail)
	values (:codigo, :cod_canjeo, :premio, :email, :nombre, :telefono, :edad, :municipio, :cod_postal, :direccion, CURRENT_TIMESTAMP, :canjeado, :estado_mail)";
	$miconexion=connectDB();
	$statement = $miconexion->prepare($consulta);
	//$statement ->setFetchMode(PDO::FETCH_ASSOC);
	$statement->bindParam(':codigo', $codigo);
	$statement->bindParam(':cod_canjeo', $cod_canjeo);
	$statement->bindParam(':premio', $idPremio);
	$statement->bindParam(':email', $email);
	$statement->bindParam(':nombre', $nombre);
	$statement->bindParam(':telefono', $telefono);
	$statement->bindParam(':edad', $edad);
	$statement->bindParam(':municipio', $municipio);
	$statement->bindParam(':cod_postal', $cod_postal);
	$statement->bindParam(':direccion', $direccion);
	$statement->bindParam(':canjeado',$cod_canjeo);
	$statement->bindParam(':estado_mail', $estadoMail);
	if($statement->execute()){
		echo json_encode($sorteo);
	} else {
		echo "error al registrar datos";
	}


}else {
	echo "error en el sorteo";

}


	

	
	/*if(!$statement->execute()){
		echo "ERROR";
		//$miconexion->close();
	}
	/*$datos=[];
	while($fila=$statement->fetch())
	{
		$datos[]=$fila;
	}
	if(count($datos)!=0){
		echo "ok";	
	} else {
		echo "error";
	}

*/

	/*
	$consulta = "select * from participantes where telefono=:;";
	$miconexion=connectDB();
	$statement = $miconexion->prepare($consulta);
	$statement ->setFetchMode(PDO::FETCH_ASSOC);
	$statement->bindParam(':num', $num, PDO::PARAM_STR);
	$statement->bindParam(':num', $num, PDO::PARAM_STR);
	$statement->bindParam(':num', $num, PDO::PARAM_STR);
	$statement->bindParam(':num', $num, PDO::PARAM_STR);
	$statement->bindParam(':num', $num, PDO::PARAM_STR);
	if(!$statement->execute()){
		echo "ERROR";
		$miconexion->close();
	}
	$datos=[];
	while($fila=$statement->fetch())
	{
		$datos[]=$fila;
	}
	if(count($datos)!=0){
		echo "existe";	
	} else {
		echo "no-existe";
	}
	*/
	} else {
		echo "error-codigo";
	}
	
}

function timeStamp($timestamp){
	$fechaHora = explode(" ", $timestamp);
	$dato = [];
	$dato["fecha"] = $fechaHora[0];
	$dato["hora"] = $fechaHora[1];
	return $dato;
}


?>
