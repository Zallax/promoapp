mostrarCarga();
$(document).ready(function(){
	ocultarCarga();
	$("#phone").intlTelInput({
    utilsScript: "js/utils.js",
    initialCountry: "es",

	});


});


	$(".edad #btn-si").on("click", function(){
		mostrarCarga();
		$(".edad").removeClass("zoomIn").addClass("zoomOut");
		setTimeout(mostrarTelefono, 500);
	});
	$("#btn-salir").on("click", function(){


		mostrarCarga();
		setTimeout(function(){
			window.location.href = "../";
		});	
		
	});
	$("#btn-siguiente").on("click", function(){
		$(this).attr("disabled","disabled");
		setTimeout(function(){
			$("#btn-siguiente").prop("disabled", false);
		}, 1000);
	//	var prefijo = 
		$("#phone").intlTelInput("getSelectedCountryData").dialCode;
		var tel = "+"+$("#phone").intlTelInput("getSelectedCountryData").dialCode+$("#phone").val();
		
		
		
/*
		var codigo = false;
		if ( $("#codigo").length > 0 ) {
		  if(valTel(tel) && valCod()){
		  	if(existeTel(tel)){
		  		mostrarRegalo(1);
		  	} else {
		  		mostrarForm();
		  	}
		  }
		} else {
			if(valTel(tel)){
				if(existeTel(tel)){
					mostrarRegalo(1)
				} else {
					mostrarForm();
				}
			}
		}
*/	

	
	var valCodigo = valCod();
	var valTelefono = valTel();
	if(valCodigo && valTelefono){
		mostrarCarga();
			comprobarTelefono(tel).done(function(datos){	
					
						console.log(datos);
						var codigo = false;
						if ( $("#codigo").length > 0 ) {
						  codigo = true;
						}

						if(datos=="existe"){
							var repetir = $("#phone").attr("repetir");		
							if(repetir=="si"){
								document.cookie="tel="+tel;
								if(codigo){
									
										var datoCodigo = $("#codigo").val();
										comprobarCodigo(datoCodigo).done(function(resultado){
											if(resultado == "valido"){
												mostrarRegalo(1);
											} else {
												errorDeCodigo(resultado);
											}

										});
									
									
										
								} else {
											mostrarRegalo(1);
								}
							} else {
								alert("Ya has participado en esta promoción");
								irAInicio();
							}
							
								
							


						} else if(datos=="no-existe"){
							var telefono = "+"+$("#phone").intlTelInput("getSelectedCountryData").dialCode+$("#phone").val();
							document.cookie="tel="+telefono;
							
							if(codigo){
									
								
									var datoCodigo = $("#codigo").val();
									
									comprobarCodigo(datoCodigo).done(function(resultado){
										if(resultado == "valido"){
											mostrarForm();
										} else {
											errorDeCodigo(resultado);
										}

									});
							
						
						
							
						} else {
							mostrarForm();
						}
						

					
				}



		
			});

			}
		});
	
			
		

	$(".formulario #btn-participar").on("click", function(){
		var validado = validarForm();
		$(this).attr("disabled","disabled");
		setTimeout(function(){
			$(".formulario #btn-participar").prop("disabled", false);
		}, 1000);
		if(validado){
			mostrarCarga();
			$(".formulario").removeClass("zoomIn").addClass("zoomOut");
			
			
			setTimeout(function(){
				mostrarRegalo("");
				
			}, 500);
		}
		
	});



function mostrarTelefono(){
	$(".edad").hide();
	mostrarCarga();

	setTimeout(function(){
		$(".telefono").addClass("animated zoomIn").removeClass("d-none");
	},500);

}

function mostrarForm(){
	$(".telefono").hide();
	ocultarCarga();

	setTimeout(function(){
		$(".formulario").addClass("animated zoomIn").removeClass("d-none");
	},500);
	

}


function mostrarRegalo(tipo){
	if(tipo==1){
		$(".telefono").hide();
		registrarParticipacion();
	} else {
		$(".formulario").hide();
		registrarDatos();
	}
	ocultarCarga();
	setTimeout(function(){
		$(".buscando").addClass("animated zoomIn").removeClass("d-none");
		setTimeout(function(){
			$(".buscando").removeClass("zoomIn").addClass("rubberBand");
		
		},1500);
		setTimeout(function(){
			$(".buscando").removeClass("rubberBand").addClass("pulse");
		
		},2500);
			setTimeout(function(){
				
				$(".lupa").addClass("animated wobble");
			},1000);
			setTimeout(function(){
				
				$(".lupa").removeClass("wobble").addClass("wobble");
			},2000);
			setTimeout(function(){
				
				$(".lupa").removeClass("wobble").addClass("animated bounceOut");
			},2600);
			setTimeout(function(){
				
				$(".regalo").addClass("animated bounceOut");
			},2800);
	},500);

	
	
		
	
}



function comprobarTelefono(num){
  return $.post("php/conexion.php", {funcion:"comprobarNum", num:num}, function(result){
    resultado = result;
  });
}




function comprobarCodigo(cod){
	return $.post("php/conexion.php", {funcion:"comprobarCodigo", cod:cod}, function(result){
	    resultado = result;
	  });
}

function mostrarPremio(){
	
	$(".buscando").removeClass("zoomIn").addClass("zoomOut");
	setTimeout(function(){
		$(".buscando").hide();
		
		$(".zona-premio").addClass("animated jackInTheBox").removeClass("d-none");

	}, 500);
	setTimeout(function(){
		$("#carga").fadeIn("500");
	},29000);
	setTimeout(function(){
		//location.reload();
		window.location.href = "../";
	},30000);

}


function irAInicio(){
	ocultarCarga();
	$("input").val("");
}

function ocultarCarga(){
	$("#carga").fadeOut("500");
}

function errorDeCodigo(tipo){
	$("#carga").fadeOut("500");
	if(tipo=="no-existe"){
		alert("El código introducido no es válido");
	}
	else if(tipo=="utilizado"){
		alert("El código introducido ya ha sido utilizado");
	} else {
		alert("Ha ocurrido un error");
		console.log(tipo);
	}


	$("#codigo").val("");
	$("#phone").val("");


}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

//VALIDACIONES

function valTel(){

	var valido = true;
	$("#phone").removeClass("is-invalid");
	$(".invalid-feedback.tel").fadeOut();
	var exp = new RegExp("^[0-9]*$");
	if($("#phone").val().length<5 || !exp.test($("#phone").val())){
		
		$("#phone").addClass("is-invalid");

		$(".invalid-feedback.tel").fadeIn();
		valido = false;
	}

	

	return valido;
}

function valCod(){
	var valido = true;

	if ( $("#codigo").length > 0 ) {
	  $("#codigo").removeClass("is-invalid");
	  $(".invalid-feedback.cod").fadeOut();
	  	if($("#codigo").val().length<=2){
	  		$("#codigo").addClass("is-invalid");
	  		$(".invalid-feedback.cod").fadeIn();
	  		valido = false;
	  	}
	  	$("#carga").fadeOut("500");
	}

	

	return valido;
}


function registrarDatos(){
	//var aDatos = [];

	var tmp = { };
	tmp["telefono"] = "+"+$("#phone").intlTelInput("getSelectedCountryData").dialCode+$("#phone").val();
	
	if($("#codigo").length!=0){
		tmp["codigo"] = $("#codigo").val().toUpperCase();
	}

	tmp["nombre"] = $("#nombre").val();

	tmp["edad"] = $("#edad").val();

	tmp["municipio"] = $("#municipio").val();

	

	tmp["direccion"] = $("#direccion").val();

	tmp["email"] = $("#email").val().toLowerCase();

	tmp["cod_postal"] = $("#codp").val();


	//aDatos.push(tmp);

	var jDatos = JSON.stringify(tmp);

	//console.log(jDatos);

	postRegistrarDatos(jDatos).done(function(result){

		if(isJSON(result)){
			var premio = JSON.parse(result);
			if(premio[0]!="-"){
				$("#n-premio").html(premio[1]);
				$(".ganador").removeClass("d-none");
				$("#img-premio").attr("src","img/premios/"+premio[0]+".jpg");
				$("#cod-usado").text($("#codigo").val().toUpperCase());
				setTimeout(mostrarGlobos,3000);

			} else {
				$(".perdedor").removeClass("d-none");
			}

			setTimeout(mostrarPremio, 3000);
		} else if(result=="error-codigo"){
			alert("Has intentado participar con un código inválido.");
			//window.location.href = "../";
		} else {
			//window.location.href = "../";
			console.log("Ha ocurrido un error");
			console.log(result);
		}
		
	});

		



}


function registrarParticipacion(){

	var telefono = "+"+$("#phone").intlTelInput("getSelectedCountryData").dialCode+$("#phone").val();

	postGetDatos(telefono).done(function(result){

		if(result!="error"){

			var datos = JSON.parse(result);
		
			$("#nombre").val(datos[0]["nombre"]);

			$("#edad").val(datos[0]["edad"]);

			$("#municipio").val(datos[0]["municipio"]);

			$("#direccion").val(datos[0]["direccion"]);

			$("#email").val(datos[0]["email"]);

			$("#codp").val(datos[0]["cod_postal"]);
			registrarDatos();
				$("#carga").fadeOut("500", function(){
						$(".buscando").addClass("animated zoomIn").removeClass("d-none");
						setTimeout(function(){
							$(".buscando").removeClass("zoomIn").addClass("rubberBand");
						
						},1500);
						setTimeout(function(){
							$(".buscando").removeClass("rubberBand").addClass("pulse");
						
						},2500);
							setTimeout(function(){
								
								$(".lupa").addClass("animated wobble");
							},1000);
							setTimeout(function(){
								
								$(".lupa").removeClass("wobble").addClass("wobble");
							},2000);
							setTimeout(function(){
								
								$(".lupa").removeClass("wobble").addClass("bounceOut");
							},2600);
							setTimeout(function(){
								
								$(".regalo").addClass("animated bounceOut");
							},2800);
						
					});
					
		}
		
		
	});

		



}

function validarForm(){
	var valido = true;
	$("#nombre").removeClass("is-invalid");
	$("#edad").removeClass("is-invalid");
	$("#email").removeClass("is-invalid");
	$("#municipio").removeClass("is-invalid");
	$("#codp").removeClass("is-invalid");
	$("#direccion").removeClass("is-invalid");
	$("#error-checks").fadeOut();
	
	if($("#nombre").val().length<=4){
		$("#nombre").addClass("is-invalid");
		
		//$(".invalid-feedback.nombre").fadeIn();
		valido = false;
	}

	var eEdad = new RegExp('^[0-9]*$');

	
	if($("#edad").val().length>3 || $("#edad").val().length==0 || $("#edad").val()<18 || !eEdad.test($("#edad").val())){
		$("#edad").addClass("is-invalid");
		
		//$(".invalid-feedback.nombre").fadeIn();
		valido = false;
	}

	if($("#municipio").val().length<=3){
		$("#municipio").addClass("is-invalid");
		
		//$(".invalid-feedback.nombre").fadeIn();
		valido = false;
	}

	if($("#codp").val().length>8 || $("#codp").val().length==0 || !eEdad.test($("#codp").val())){
		$("#codp").addClass("is-invalid");
		
		//$(".invalid-feedback.nombre").fadeIn();
		valido = false;
	}

	if($("#direccion").val().length<=4){
		$("#direccion").addClass("is-invalid");
		
		//$(".invalid-feedback.nombre").fadeIn();
		valido = false;
	}


	var eMail = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);

	if(!eMail.test($("#email").val().toLowerCase())){
		$("#email").addClass("is-invalid");
		
		//$(".invalid-feedback.nombre").fadeIn();
		valido = false;
	}
	if(!$("#check1").prop('checked') || !$("#check2").prop('checked')){
		valido = false;
		$("#error-checks").fadeIn().removeClass("d-none");
	}

	return valido;
}


function postRegistrarDatos(datos){
	
	return $.post("php/conexion.php",{funcion:"registrarDatos", datos:datos}, function(result){
		resultado = result;
	});
}

function postGetDatos(telefono){
	
	return $.post("php/conexion.php",{funcion:"getDatos", telefono:telefono}, function(result){
		resultado = result;
	});
}
function mostrarCarga(){
	$("#carga").fadeIn();
	$('html, body').css({
		    'overflow': 'hidden',
		    'height': '100%'
		});
}

function ocultarCarga(){
	$('html, body').css({
	    'overflow': 'auto',
	    'height': 'auto'
	});
	$("#carga").fadeOut();
}

function isJSON(str) {

    if( typeof( str ) !== 'string' ) { 
        return false;
    }
    try {
        JSON.parse(str);
        return true;
    } catch (e) {
        return false;
    }
}

function mostrarGlobos(){
	$(".g1, .g2, .g3").hide().removeClass("d-none").fadeIn();
	setTimeout(function(){
		$(".g1, .g2, .g3").fadeOut().addClass("d-none");
	},4000);
}




/*
function existeTel(tel){
	var existe = false;

	comprobarTelefono(tel).done(function(datos){
		if(datos=="existe"){
			existe = true;
			document.cookie="tel="+tel;

		}
		return "Hola";
	});

	return existe;
}*/
