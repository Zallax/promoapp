<?php
    
    require_once("php/conexion.php");
    $repetirTel = 'repetir="si"';
    if ($variasParticipaciones==true){
      $repetirTel = 'repetir="no"';
    }
    insert("update config set valor = valor+1 where id = 9");

?>

<html lang="es">
<head>

        <meta charset="utf-8">
        <link rel="icon" type="image/png" href="img/favicon.png">

            <link rel="stylesheet" href="dash/vendor/bootstrap/css/bootstrap.min.css">
            <link rel="stylesheet" href="css/intlTelInput.css">

             <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no maximum-scale=1" user-scalable="no">
            <title><?php echo $nombrePromo; ?></title>
            <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
                  rel="stylesheet">
            <link rel="stylesheet" href="css/general.css">
             <link rel="stylesheet" href="css/animate.css">
            <style type="text/css">
               .btn-info{
                 background-color:<?php echo $color ?> !important;
               }
               .border-color{
                 border-color:<?php echo $color ?> !important;
               }
               .check::before{
                 border:1px solid <?php echo $color ?> !important;
               }
               .cabecera{
                background-color:<?php echo $color ?> !important;
               }

               #codigo{
                font-weight:bold;
                text-transform:uppercase;
                color:<?php echo $color ?> !important;
               }
               .color-p{
                color: <?php echo $color ?> !important;
               }

            </style>
      </head>
      <body>
      <!-- Modal -->
      <div class="modal fade" id="modalTerminos" tabindex="-1" role="dialog" aria-labelledby="modalTerminosTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="modalTerminosTitle">Bases legales de la promoción</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <?php echo $basesLegales; ?>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Cerrar</button>
              
            </div>
          </div>
        </div>
      </div>

      <img src="img/globo1.png" class="g1 d-none">
      <img src="img/globo2.png" class="g2 d-none">
      <img src="img/globo3.png" class="g3 d-none">

      <section class="cabecera">
        <img src="img/cabecera.png">

      </section>

      <?php

        if($estado){


      ?>
        <!--ZONA MAYOR EDAD
        <div class="edad animated zoomIn">
          <div class="row text-center no-gutters">
            <div class="col-12 mb-5">
              <h1>¿Eres mayor de 18 años?</h1>
            </div>
            <div class="col-6 text-right">
                
                 <button class="btn btn-primary btn-lg boton-edad rounded-circle">
                  NO
                </button>
            </div>
            <div class="col-6 text-left">
               <button class="btn btn-info btn-lg boton-edad rounded-circle" id="btn-si">
                 SI
               </button>
            </div>
          </div>
        </div>-->
        <!--Zona Teléfono-->
          <div class="telefono contenido text-center mb-3">
           
          <div class="row">
            <div class="col-12">
             <?php
              if ($codigo){




             ?>
               <h3>INTRODUCE TU CÓDIGO</h3>
            <input type="text" class="form-control form-control-lg border border-color campo" id="codigo" placeholder="Código">
            <div class="invalid-feedback cod">
                     Introduce un código para continuar
                   </div>

              <?php
              }

              ?>

               <h3 class="mt-5 ">INTRODUCE TU TELÉFONO</h3>
              <input type="number" class="form-control form-control-lg border border-color campo mb-5" <?php echo $repetirTel; ?> placeholder="TELÉFONO" id="phone">
        <div class="invalid-feedback tel">
                 Introduce un teléfono para continuar
               </div>
              
              
              <button class="btn btn-info btn-block campo mt-5" id="btn-siguiente">CONTINUAR</button>
            </div>
            
          </div>
        </div>

        <!--ZONA FORMULARIO-->
        <div class="formulario contenido d-none">
          <span class="text-center"><h3>¡Introduce tus datos y participa!</h3></span>
        <div class="row">
          <div class="col-12">
            <div>
              <input type="text" class="form-control form-control-lg border border-color campo" id="nombre" placeholder="Nombre completo">
              <div id="errorNombre" class="invalid-feedback">
                       Introduce tu nombre completo
              </div>
            </div>
            <div>
              <input type="number" class="form-control form-control-lg border border-color campo" id="edad" placeholder="Edad">
              <div class="invalid-feedback">
                       Introduce una edad válida
              </div>
            </div>
            <div>
              <input type="text" class="form-control form-control-lg border border-color campo" id="municipio" placeholder="Municipio">
              <div class="invalid-feedback">
                       Introduce un municipio válido
              </div>
            </div>
            <div>
              <input type="number" class="form-control form-control-lg border border-color campo" id="codp" placeholder="Código Postal">
              <div class="invalid-feedback">
                       Introduce un Código Postal válido
              </div>
            </div>
            <div>
              <input type="text" class="form-control form-control-lg border border-color campo" id="direccion" placeholder="Dirección">
              <div class="invalid-feedback">
                       Introduce una direccion válida
              </div>
            </div>
            <div>
              <input type="mail" class="form-control form-control-lg border border-color campo" id="email" placeholder="Email">
              <div class="invalid-feedback ">
                       Introduce un Email válido
              </div>
            </div>
            
           
            <div class="custom-control custom-checkbox mb-2 mt-2">
              <input type="checkbox" class="custom-control-input" id="check1">
              <label class="custom-control-label check" for="check1"><span style="opacity:0%;">.</span> Al participar aceptas los <a href="#" data-toggle="modal" data-target="#modalTerminos">bases legales de la promoción
              </a></label>
            </div>

            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="check2">
              <label class="custom-control-label check" for="check2"><span style="opacity:0%;">.</span> Acepto los términos de uso y quiero recibir ofertas especiales, información de productos y servicios ofertados por <?php echo $nombreEmpresa ?></label>
            </div>

            <div id="error-checks" class="text-danger d-none">
              Es obligatorio aceptar todos los términos y condiciones.
            </div>

            <button class="btn btn-info btn-block campo mb-2" id="btn-participar">¡PARTICIPAR!</button>
          </div>
          
        </div>
      </div>


      	<!--ZONA BUSCANDO-->
      	<div class="buscando d-none">
      		<div class="zona-regalo text-center">
	      		<img src="img/gift.png" class="regalo">
	      		<div class="lds-css ng-scope lupa"><div style="width:100%;height:100%" class="lds-magnify"><div><div><div></div><div></div></div></div></div></div>
      		</div>
      	</div>

        <div class="zona-premio contenido d-none text-center">
         <div class="ganador d-none">
          <h1 style="font-size:2rem;">¡ENHORABUENA!</h1>
          <h4>Has ganado:</h4>
          <h4><span class="text-danger" id="n-premio"></span></h4>
          <img width="300px" id="img-premio" src="">
          <p class="color-p">Código: <span class="font-weight-bold" id="cod-usado"></span></p>
          <p class="text-primary">Para reclamar tu premio, acércate a nuestro centro junto a tu código ganador</br><small class="text-primary">Es indispensable conservar el ticket de código hasta la entrega del regalo</small></p>
         

         </div>
         <div class="perdedor d-none">
          <h1>¡VAYA!</h1>
          <h4>No has conseguido ningún premio</h4>
         
          <img width="200px" src="img/sad.png" class="mt-5 mb-5 animated shake">
         </div>
         <button class="btn mt-4 mb-2 btn-info btn-block rounded" id="btn-salir">SALIR</button>
        </div>
        <?php
            } else {
              echo '<div class="text-center mt-3"><h1>Esta promoción ha finalizado</h1></div>';
            }

        ?>

        <section id="carga" style="background-color:<?php echo $color;?>;">
         
          <div class="pl-spinner">
                      <div class="pl-spinner-bubble"></div>
                      <div class="pl-spinner-bubble2"></div>
                  </div>
        </section>

      	

      	<script
          src="https://code.jquery.com/jquery-3.4.1.js"
          
          crossorigin="anonymous"></script>
      	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
      	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script src="js/intlTelInput-jquery.js"></script>
        <script src="js/utils.js"></script>
 
        <script type="text/javascript" src="js/general.js"></script>


      </body>
</html>